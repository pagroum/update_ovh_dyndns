#!/usr/bin/env python
# -*- coding: utf-8 -*-

import pycurl
import sys
import re
from StringIO import StringIO

MY_FQDN = 'mydomain.fqdn'
MY_USER = 'user'
MY_PASSWORD = 'superpassword'

MY_IP_URL = 'ipecho.net/plain'
OVH_URL = 'http://www.ovh.com/nic/update?system=dyndns&hostname=%s&myip=%s'
IP_V4_FORMAT = "((([0-9]{1})\.)|([0-9]{1}[0-9]{1}\.)|([0-1]{1}[0-9]{1}[0-9]{1}\.)|([2]{1}[0-4]{1}[0-9]{1}\.)|([2]{1}[5]{1}[0-5]{1}\.)){3}((([0-9]{1}))|([0-9]{1}[0-9]{1})|([0-1]{1}[0-9]{1}[0-9]{1})|([2]{1}[0-4]{1}[0-9]{1})|([2]{1}[5]{1}[0-5]{1}))"

PREFIX_PRIVATE_NETWORK = [
    '10.',
    '172.16.',
    '172.17.',
    '172.18.',
    '172.19.',
    '172.20.',
    '172.21.',
    '172.22.',
    '172.23.',
    '172.24.',
    '172.25.',
    '172.26.',
    '172.27.',
    '172.28.',
    '172.29.',
    '172.30.',
    '172.31.',
    '192.168.'
]


def call_url(url, user=None, password=None):
    buffer = StringIO()
    my_curl = pycurl.Curl()
    my_curl.exception = None
    my_curl.setopt(my_curl.URL, url)
    my_curl.setopt(my_curl.WRITEFUNCTION, buffer.write)
    my_curl.setopt(my_curl.FOLLOWLOCATION, True)
    if user and password:
        my_curl.setopt(my_curl.USERPWD, user + ':' + password)
    try:
        my_curl.perform()
    except pycurl.error as pycurl_error:
        if pycurl_error.args[0] == pycurl.E_COULDNT_CONNECT and my_curl.exception:
            print 'The curl failed on url "%s "with the error : %s' % (url, my_curl.exception)
            sys.exit(2)
        else:
            print 'The curl failed on url "%s "with the error : %s' % (url, pycurl_error)
            sys.exit(2)
    http_code = my_curl.getinfo(my_curl.RESPONSE_CODE)
    my_curl.close()
    if http_code != 200:
        print 'The call to "%s" return an http error %s' % (url, http_code)
        sys.exit(2)
    return buffer.getvalue().strip()


def is_valid_ip_format(ip):
    if not ip or not re.match(IP_V4_FORMAT, ip):
        return False
    return True


def is_private_ip(ip):
    for prefix in PREFIX_PRIVATE_NETWORK:
        if ip.startswith(prefix):
            return True
    return False


def main():
    print 'Start to update your dynamic DNS'
    # Fisrt, get your public IP
    my_ip = call_url(MY_IP_URL)
    # Then, check if this IP if valid and not private
    if not is_valid_ip_format(my_ip):
        print 'ERROR : There is an invalid IP format : %s' % my_ip
        sys.exit(2)
    if is_private_ip(my_ip):
        print 'ERROR : There is private IP : %s' % my_ip
        sys.exit(2)
    
    print 'Your public IP is : %s. Will send it to the DynDNS service.' % my_ip
    # Then send it to the dynDNS service
    computed_url = OVH_URL % (MY_FQDN, my_ip)
    result = call_url(computed_url, user=MY_USER, password=MY_PASSWORD)
    
    if result.startswith('nochg'):
        print 'There is no change, the IP was already up to date !'
    else:
        print 'Update result : %s' % result
    print 'Bye !'


if __name__ == '__main__':
    main()
